import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-convertion',
  templateUrl: './convertion.component.html',
  styleUrls: ['./convertion.component.less'],
})
export class ConvertionComponent implements OnInit {
  json: string;
  input: string;
  ignored: string;

  constructor() {}

  ngOnInit(): void {
    this.json = '';
    this.input = '';
    this.ignored = '';
  }

  getIgnoredValue(event) {
    this.ignored = event.target.value;
    this.ready();
  }

  getInputValue(event) {
    this.input = event.target.value;
    this.ready();
  }

  ready() {
    if (this.ignored != '' && this.input != '') {
      this.json = '{\n' + this.convertValueToJson(this.input.split(');'));
    }
  }

  convertValueToJson(values: string[]): string {
    let json = '';
    values.forEach((value, index, arr) => {
      value = value.trim();

      //Si contiene los caracteres a elimnar se procede a buscarlos y eliminarlos
      let lstIgnored: string[];
      if (this.ignored.includes(',')) {
        lstIgnored = this.ignored.split(',');
        lstIgnored.forEach((ignoredValue) => {
          value = value.replace(ignoredValue.trim(), '');
        });
      } else if (value.includes(this.ignored)) {
        value = value.replace(this.ignored, '');
      }

      let nombreValor = value.split('(', 2);

      if (nombreValor[1] != undefined) {
        if (nombreValor[1].charAt(0) != '"') {
          nombreValor[1] = '"' + nombreValor[1] + '"';
        }
        let char = nombreValor[0].charAt(0).toLocaleLowerCase();
        nombreValor[0] =
          char + nombreValor[0].substring(1, nombreValor[0].length);
        if (arr[index + 1]) {
          json = json + '  "' + nombreValor[0] + '": ' + nombreValor[1] + ',\n';
        } else {
          json = json + '  "' + nombreValor[0] + '": ' + nombreValor[1];
        }
      }
    });

    return json + '\n}';
  }
}
