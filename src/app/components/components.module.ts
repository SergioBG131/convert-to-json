import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ConvertionComponent } from './convertion/convertion.component';

@NgModule({
  declarations: [
    ConvertionComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  exports: [ConvertionComponent],
})
export class ComponentsModule { }